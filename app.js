const { App } = require('@slack/bolt');
const { onWorkFlowPublished } = require('./CIBot')

const DEFAULT_PORT = 3000

const app = new App({
    token: process.env.SLACK_BOT_TOKEN,
    signingSecret: process.env.SLACK_SIGNING_SECRET,
    socketMode: true,
    appToken: process.env.SLACK_APP_TOKEN,
    port: process.env.PORT || DEFAULT_PORT
  })

// Warning! On September 12, 2024, Slack will deprecate workflow_published event
app.event('workflow_published', onWorkFlowPublished)

(async () => { await app.start() })()
