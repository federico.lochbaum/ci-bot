
const errorMessage = message => `Ha ocurrido un error con la autenticación, el mensaje de error obtenido fue: ${message}`
const successMessage = link => `Se ha generado exitosamente un link de autenticación, para continuar, navegá a: ${link}`

const replyMessage = ({ message }) => message

module.exports = { errorMessage, successMessage, replyMessage }