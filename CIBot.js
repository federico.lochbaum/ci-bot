// Following this documentation https://api.slack.com/apis/connections/events-api

const { replyMessage } = require('./message')
const { buildErrorResponse, buildOKResponse } = require('./response')
const { getEventError } = require('./validate')

const field = 'Motivo de consulta'

const getWorkFlowType = ({ somePath }) => somePath[somePath] // TODO: Esto tiene que agarrar el tipo de evento del formulario

const getFacePhiLink = (event, client) => EVENTS[getWorkFlowType(event)](event, client).then(buildOKResponse).catch(buildErrorResponse)

const facePhiAuthenticationLink = (event, client) => getEventError(event, user) || getFacePhiLink(event, client)

const onWorkFlowPublished = async ({ event, client, say, next }) => EVENTS[getWorkFlowType(event)] ? 
    say(replyMessage(await facePhiAuthenticationLink(event, client))) :
    next()

module.exports = { onWorkFlowPublished }
