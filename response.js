const { errorMessage, successMessage } = require('./message')

const RESPONSE = { error: 'error', success: 'success' }

const buildResponse = (type, message ) => ({ type, message })

const buildErrorResponse = ({ message }) => buildResponse(RESPONSE.error, errorMessage(message))

const buildOKResponse = ({ body }) => buildResponse(RESPONSE.success, successMessage(body.link)) // TODO: tenemos que ver como lo estamos devolviendo desde el bff ( response )

module.exports = { buildErrorResponse, buildOKResponse }