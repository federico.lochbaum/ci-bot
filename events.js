
const EVENT_TYPES = {
    CardCancellation: 'Baja de tarjeta',
    InsuranceCancellation: 'Baja de seguro'
}

const EVENTS = {
    [EVENT_TYPES.CardCancellation]: async (event, client) => {}, // TODO: implementa la llamada http 
    [EVENT_TYPES.InsuranceCancellation]: async (event, client) => {} // TODO: implementa la llamada http
}

module.exports = { EVENTS }